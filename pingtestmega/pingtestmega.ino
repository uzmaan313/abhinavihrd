int s, l, r, a;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);
  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
}

void loop() {
  long d1, d2, d3, cm1, cm2, cm3;
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(5);
  digitalWrite(8, LOW);
  d1 = pulseIn(7, HIGH);
  cm1 = microsecondsToCentimeters(d1);
  Serial.print(cm1);
  Serial.print("cm1");
  Serial.println();
  digitalWrite(9, LOW);
  delayMicroseconds(2);
  digitalWrite(9, HIGH);
  delayMicroseconds(5);
  digitalWrite(9, LOW);
  d2 = pulseIn(10, HIGH);
  cm2 = microsecondsToCentimeters(d2);
  Serial.print(cm2);
  Serial.print("cm2");
  Serial.println();
  digitalWrite(11, LOW);
  delayMicroseconds(2);
  digitalWrite(11, HIGH);
  delayMicroseconds(5);
  digitalWrite(11, LOW);
  d3 = pulseIn(12, HIGH);
  cm3 = microsecondsToCentimeters(d3);
  Serial.print(cm3);
  Serial.print("cm3");
  Serial.println();
  delay(100);
}

long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}
