import matplotlib.pyplot as plt
import numpy as np
def findMinDiff(arr):
	n=len(arr)
	diff = 10**20
	for i in range(n-1):
		for j in range(i+1,n):
			if abs(arr[i]-arr[j]) < diff and abs(arr[i]-arr[j]) >= 1:
				diff=abs(arr[i]-arr[j])
				print(diff)

	return diff

x=[[1,2,3,4,5,6,7,8,9,2,3,4,6,7],[1,3,3,4,5,4,4,2,1,1,2,3,3,2]]
lm=x[0].index(min(x[0]))
rm=x[0].index(max(x[0]))
#print(rm)
xdiff=int(findMinDiff(x[0]))
ydiff=int(findMinDiff(x[1]))
xn=np.array(x)
xs=xn[:,xn[0].argsort()]
ys=xn[:,xn[1].argsort()]
print(xn)
print(ys)
print(xs)
ty=np.amax(ys[1])
typ=np.where(xs[0]==ty)
pw=list(zip(xs[0],xs[1]))
l=len(xn[0])
typi=typ[0][0]
xsp=pw[0][0]
ysp=0
xo=np.empty(0)
for i in range(0,typi+1):
	if(pw[i][0]>xsp):
		xsp=pw[i][0]
	if(xsp==pw[i][0]):
		if(pw[i][1]>ysp):
			ysp=pw[i][1]
			xom=(xsp,ysp)
			xo=np.concatenate((xo,xom),axis=0)
			print(xo)
###################################
#ry=np.amax(xs[0])
#ryp=np.where(ys[0]==ry)
pw=list(zip(xs[0],xs[1]))
#rypi=ryp[0][0]
xsp=pw[l-1][0]
ysp=0
print(pw)
xo2=np.empty(0)
for i in range(l-1,typi,-ydiff):
	print(i)
	print(xsp)
	print(ysp)
	if(pw[i][0]<xsp):
		xsp=pw[i][0]
	if(xsp==pw[i][0]):
		if(pw[i][1]>ysp):
			print(pw[i][0])
			ysp=pw[i][1]
			xom=(xsp,ysp)

			xo=np.concatenate((xo,xom),axis=0)
			print(xo2)
xo=xo.reshape(int(len(xo)/2),2)
xo=np.transpose(xo)	
#xo=np.concatenate((xo,xo2),axis=0)
print(xo)
xo=xo[:,xo[0].argsort()]
print(xo)
#plt.plot(xo[0],xo[1], color= "blue")
xl=np.array([[9,1],[1,1]])
#plt.plot(xl[0],xl[1], color= "blue")
plt.scatter(x[0],x[1], label= "stars", color= "green", s=10) 
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.title('Largest convex polygon')
plt.show()
